#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include "DHT.h"
#include "LedControl.h"
 
#define DHT1PIN 2 //define sensor1 digital input pin
#define DHT1TYPE DHT11 //define sensor type
LedControl lc = LedControl(12, 13, 10, 1); //sevenSeg
bool fboot = true;
bool erroMode = false;
int buzzerPin = 8;      //Define buzzerPin
int coolerControll = 3; //Define cooler (LED) pin
int sprinklerControll = 11 ;
int lightsControll = 7;

DHT dht1(DHT1PIN, DHT1TYPE);

 
void setup() {
 
  Serial.begin(115200);                            //Serial connection
  Serial.println("Connecting to home network ...");
  WiFi.begin("CEH", "A775f462");   //WiFi connection
 
  while (WiFi.status() != WL_CONNECTED) {  //Wait for the WiFI connection completion
 
    delay(500);
    Serial.println("Waiting for connection");
 
  }
  pinMode(5, INPUT);
  Serial.println("Connected");
  Serial.println("Reading data from sensors");
  Serial.println("Please wait...");
  dht1.begin();
 
}
 
/*void readTemp(float *temp , float *humidity ,int *light)
{
  
  float h = dht1.readHumidity();
  float t = dht1.readTemperature();
  int l = digitalRead(5);
  for (int i = 0; i < 11; i++)
  {
    delay(500);
    t = t + dht1.readTemperature();
    h = h + dht1.readHumidity();
    l = l + digitalRead(5);
  }
  if (isnan(h) || isnan(t))
  {
    Serial.println("can not read the DHT sensor");
  }
  else{
  temp =  t;
  humidity = h;
  light = l;    
  }
  
}*/

void loop() {
  Serial.println("hello");
  float temp , humidity ;
//  readTemp(&temp , &humidity , &light);
  temp = 25;
  humidity = 55;
  bool light = false;

  Serial.println("Reading sensors Done. Result is:");
  //Serial.println("Temp:%fC ,Humidity:%f\% ,Light:%d ",temp,humidity,light);
  Serial.println("Sending data to server...");

  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
 
    StaticJsonBuffer<300> JSONbuffer;   //Declaring static JSON buffer
    JsonObject& JSONencoder = JSONbuffer.createObject(); 
    //JSONencoder["DeviceSerialNum"] = temp;
    //TODO: add DSN^ stead of UserId
    JSONencoder["UserId"] = 0;
    JSONencoder["Temp"] = temp;
    JSONencoder["Humidity"] = humidity;
    JSONencoder["Light"] = light;
/*
    JsonArray& values = JSONencoder.createNestedArray("values"); //JSON array
    values.add(20); //Add value to array
    values.add(21); //Add value to array
    values.add(23); //Add value to array
 
    JsonArray& timestamps = JSONencoder.createNestedArray("timestamps"); //JSON array
    timestamps.add("10:10"); //Add value to array
    timestamps.add("10:20"); //Add value to array
    timestamps.add("10:30"); //Add value to array
 */
    char JSONmessageBuffer[300];
    JSONencoder.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
    Serial.println(JSONmessageBuffer);
 
    HTTPClient http;    //Declare object of class HTTPClient
    WiFiClient client;
    http.begin( client,"http://10.13.13.4:3960/device/Logs");      //Specify request destination
    http.addHeader("Content-Type", "application/json");  //Specify content-type header
 
    int httpCode = http.POST(JSONmessageBuffer);   //Send the request
    //String payload = http.getString();                                        //Get the response payload
 
   // Serial.println(httpCode);   //Print HTTP return code
    //Serial.println(payload);    //Print request response payload
 
    http.end();  //Close connection
 
  } else {
 
    Serial.println("Error in WiFi connection");
 
  }
 
  delay(30000);  //Send a request every 30 seconds
 
}
